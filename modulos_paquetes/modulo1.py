def promedio(valores: list):
	"""Calcula el promedio de una lista de enteros o decimales.
	:param valores: <list> proporcione una lista.
	:return: result: <float> promedio de los valores de la lista.
	"""
	suma = 0
	for i in valores:
		suma += i

	return suma / len(valores)


def descuento_20(valor: int):
	"""Calcula del 20% de descuento de un valor determinado.
	:param valor: <int> valor entero
	:return: result: <int> resultado.
	"""
	return valor * 0.8

