def saludar(nombre: str):
	"""Saludar a una persona
	:param nombre: <str> Nombre de la persona a saludar
	:return: result: <str> mensaje de bienvenida
	"""
	return f"Hola {nombre}"
