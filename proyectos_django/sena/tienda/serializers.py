from .models import *
from rest_framework import serializers


class CategoriaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Categoria
		# fields = ['id', 'nombre', 'descripcion']
		fields = '__all__'


class ProductoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Producto
		# fields = ['id', 'nombre', 'precio', 'fecha_compra', 'categoria', 'stock', 'foto'] 
		fields = '__all__'


class UsuarioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuario
		fields = ['id', 'foto', 'nombre', 'apellido', 'nick', 'rol']
  

class VentaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Venta
		fields = ['id', 'fecha_venta', 'usuario', 'estado']
  
  
class DetalleVentaSerializer(serializers.ModelSerializer):
	class Meta:
		model = DetalleVenta
		fields = ['id', 'venta', 'producto', 'cantidad', 'precio_historico']