# Por definición, el factorial de 0! = 1
# Calcular el factorial de un número positivo, así:
# Ejemplo: 7! =  7 x 6 x 5 x 4 x 3 x 2 x 1 =
# Ejemplo: 3! =  3 x 2 x 1 = 6

n = int(input("Digite un num para saber su factorial: "))
total = 1
cont = n
while cont > 1:
    total = total * cont
    print(cont)
    cont -= 1

print(f"{n}! = {total}")
