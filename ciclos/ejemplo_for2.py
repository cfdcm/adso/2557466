# Función range(start, stop, step)

# range(0, 5)           [0,1,2,3,4]
# range(5)              [0,1,2,3,4]

# Cuando no se quiere iniciar en cero.

# range(1, 5)           [1,2,3,4]
# range(3, 5)           [3,4]
# range(18, 200)        [18,19, ..., 199]

# range(1, 22, 3)       [1, 4, 7, 10, 13, 16, 19]
# range(1, 23, 3)       [1, 4, 7, 10, 13, 16, 19, 22]
# range(1, 18, 3)       [1, 4, 7, 10, 13, 16]

# range(7, 5)           []

# cuenta regresiva

# range(8, 3, -1)           [8,7,6,5,4]
# range(3, -2, -1)          [3,2,1,0,-1]
# range(3, 0, -1)           [3,2,1]
# range(3, -1, -1)          [3,2,1,0]

# range(3, 8, -1)           []

print(list(range(8, 3, -1)))



"""
for i in range(-5, 5):      # [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4]
    pass
"""