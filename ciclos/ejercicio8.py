"""
Dados 5 números, indicar el mayor y el menor de todos.
Nota: usar while.
"""

mayor = 0
menor = 0
cont = 1
while cont <= 5:
    num = int(input(f"Digite un num {cont}: "))
    if cont == 1:
        mayor = num
        menor = num
    else:
        if num > mayor:
            mayor = num

        if num < menor:
            menor = num
    cont += 1
print(f"Mayor: {mayor}, Menor: {menor}")
