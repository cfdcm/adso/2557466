# Uso de listas con for o while.
"""
# 1
for i in range():
    pass

# 2
p = "hola"
for i in p:
    pass
"""
# 3
# Read - Retrieve    CRUD
frutas = ["cereza", "manzana", "pera", "guayaba", "piña"]
# print(frutas[0])
# print(frutas[1])
# print(frutas[2])

for i in frutas:
    print(i)


tam = len(frutas)
print(tam)
for i in range(tam):
    print(frutas[i])
