"""
Desarrolle un algoritmo que permita leer un valor entero
positivo N, solicite al usuario esa cantidad de sueldos.
Imprimir el sueldo promedio.

Nota: Usar do-while

"""
n = int(input("Digite un num entero positivo: "))
if n <= 0:
    print("Error, digite valores positivos...")
else:
    # contador
    cont = 1
    # acumulador
    suma = 0
    while True:
        if cont <= n:
            sueldo = int(input(f"Digite el sueldo {cont}: "))
            suma += sueldo
        else:
            break

        cont += 1

    promedio = suma / n
    print(f"El promedio de los sueldos es {promedio}")