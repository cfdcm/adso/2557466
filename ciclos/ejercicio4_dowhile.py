# Preguntar al usuario números hasta que diga "salir".
# Mostrar la sumatoria de ellos.
# Usar do-while

suma = 0
while True:
    num = input("Digite un número o la palabra 'salir' para finalizar: ").lower()
    if num == "salir":
        # romper o terminar el ciclo
        break
    else:
        suma = suma + int(num)

print(f"La suma de todos es: {suma}")

