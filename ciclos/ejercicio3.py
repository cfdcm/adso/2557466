
# pedir el número del mes (1-12) y no dejar pasar hasta que esté correcto.
# imprimir mes.

num = int(input("Digite un mes, en números: "))

while num < 1 or num > 12:
    num = int(input("Digite un mes, en números: "))

print(f"Su mes es: {num}")
