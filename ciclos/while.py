# Mientras que:  while. 
# Bucle, pasada, ciclo, iteración, Vuelta
# loop, iteration

# Crear un ciclo que pida 3 edades.
# Modo: Iteraciones. Definidas

# Contador: Variable que aumenta o dismunuye su valor con un salto específico.
cont = 0
while cont < 3:
    edad = int(input(f"Digite la edad {cont}: "))
    cont += 1
    
print("Continuar con el algoritmo....")

