# Capturar los datos personales de 3 aprendices, en una matriz.
# Pidiendo, cédula, nombre y edad.
# Luego leer la matriz y calcular el promedio de las edades.-

aprendices = []
for x in range(3):
    datos = []  # Creo un vector vacío
    ced = int(input("Digite la cédula: "))
    nombre = input("Digite el nombre: ")
    edad = int(input("Digite la edad: "))
    datos.append(ced)       # Guardo en vector datos
    datos.append(nombre)    # Guardo en vector datos
    datos.append(edad)      # Guardo en vector datos

    # Guardo todo el vector dato ... en aprendices
    aprendices.append(datos)

suma = 0
for i in aprendices:
    suma += i[2]

print(f"El promedio es {suma/len(aprendices):.2f}")


suma = (aprendices[0][2]) + (aprendices[1][2]) + (aprendices[2][2])
print(f"El promedio es {suma/3:.2f}")




