# Mismo ejercicio anterior controlando la N.

# Controlar la n
while True:
    n = int(input("Digite un num entero positivo: "))
    if n > 0:
        break
    else:
        print("Error: digite un num mayor que cero...")

print(f"Su num es {n}")

# empieza el algoritmo
suma = 0
cont = 1
while cont <= n:
    edad = int(input(f"Digite la edad {cont}: "))
    suma += edad
    cont += 1
print(f"La suma de todas las edades es {suma}")
