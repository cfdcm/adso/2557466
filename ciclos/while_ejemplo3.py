# do-while
a = 20

while True:
    print("Hola")
    a += 1

    if a >= 5:
        break


# do - while
a = 20
do = True

while do or (a < 5):
    do = False

    print("Hola")
    a += 1

