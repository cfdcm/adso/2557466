# Usar ciclo FOR.
# Pedir por pantalla, usuario y contraseña. Si concuerdan, mostrar mensaje de bienvenida. Si no, mostrar mensaje "usuario o clave incorrectos".

# Nota: Máximo 3 intentos, finalizar programa.

ok = False
for i in range(3):
    user = input("Digite su usuario: ")
    passw = input("Digite su contraseña: ")

    if user == "admin" and passw == "12345":
        ok = True
        break
    else:
        print("Usuario o contraseña incorrectos...")
else:
    print("Usted ha superado el número de intentos permitido. Fin")

if ok:
    print("Bienvenido!!")
