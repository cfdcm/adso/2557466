# Preguntar al usuario números hasta que diga "salir".
# Mostrar la sumatoria de ellos.

num = input("Digite un número o la palabra 'salir' para finalizar: ")

suma = 0        # acumulador

while num != "salir":
    suma += int(num)
    num = input("Digite un número o la palabra 'salir' para finalizar: ")


print(f"Gracias!! La suma de todos es: {suma}")
