# Crear un login para la aplicación, que verifique si el usuario y la contraseña son correctos.
# Usuario: admin
# Clave: 12345
# Darle la bienvenida.
# Máximo 3 intentos.
# Al loguearse correctamente mostrar el número de errores.

usuario = input("Digite su nombre de usuario: ")
passw = input("Digite su contraseña: ")

intentos = 0

while (usuario != "admin" or passw != "12345") and (intentos < 3):
    intentos += 1
    print(f"Usuario o contraseña incorrectos. Intentos {intentos}")

    usuario = input("Digite su nombre de usuario: ")
    passw = input("Digite su contraseña: ")


if intentos < 3:
    print("Bienvenido admin")
    if intentos != 0:
        print(f"Usted digitó sus datos mal, {intentos} veces.")
else:
    print("Bloqueo de Cuenta...")
    print(f"Usted digitó sus datos mal, {intentos} veces.")
