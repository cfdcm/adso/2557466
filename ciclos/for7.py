# Matrices, Vectores (Listas) multidimensionales
aprendices = [
    [999, "Andrés", "Martínez"],
    [555, "María", "Ossa"],
    [333, "Leidy", "Chavarría"]
]
print("="*40)
print("Cédula2\t\tNombre\t\t\tApellido")
print("="*40)
for i in aprendices:
    for datos in i:
        print(datos, end="\t\t\t")
    print("\n----------------------------------------")


"""
tam1 = len(aprendices)
# print("="*40)
# print("Cédula\t\tNombre\t\t\tApellido")
# print("="*40)
for x in range(tam1):   # [0,1,2]
    tam2 = len(aprendices[x])
    for y in range(tam2):
        print(aprendices[x][y], end="\t\t\t")
    print("\n----------------------------------------")
"""