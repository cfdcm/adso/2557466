# Averiguar si un número digitado por pantalla es primo o no

n = int(input("Digite un num para saber si es primo: "))

cuenta = 0
cont = 2
while cont < n:
    print(cont)
    if n % cont == 0:
        cuenta += 1
        print("No es primo...")
        break
    cont += 1

if cuenta == 0:
    print("Es primo")
