# Uso del break, continue, else del for.....
# Lance dos dados (aleatorios), 20 veces, pero si sale pinini, termine el ciclo.
# Debe ir acumulando la suma de los dos dados. Excepto cuando saque doble seis (Este no se debe acumular).
# Imprimir la suma total y cuántas iteraciones se dieron (sin contar doble seis ni pinini).

import random
total = 0
cont = 0
pinini = True
for i in range(20):
    dado1 = random.randint(1, 6)
    dado2 = random.randint(1, 6)
    suma = dado1 + dado2
    print(f"D1: {dado1}    D2: {dado2} = {suma}")

    if suma == 2:   # pinini
        break

    if suma == 12:  # doble seis
        continue

    total += suma
    cont += 1
else:
    pinini = False
    print("No hubo pininis.")

if pinini:
    print("El ciclo se interrumpió por pinini")

print(f"La cantidad de iteraciones reales fueron: {cont}\nTotal {total}")

