# Calcular el factorial de N. (Pedido por pantalla).
# Utilice cuenta regresiva

# 5! = 5x4x3x2x1 = ....
# 20! = 20x19 ... x3x2x1 = ....

n = int(input("Digite un num, para calcular el factorial: "))

if n == 0:
    print("El factorial de 0! = 1")
else:
    producto = 1
    for i in range(n, 0, -1):
        producto = producto * i

    print(f"El factorial de {n}! = {producto}")

