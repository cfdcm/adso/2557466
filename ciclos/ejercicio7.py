# Pedir el estado de cinco semáforos. Asegúrese que sea válido. Controlar.
# Rojo - Amarillo - Verde
# Decir un mensaje según estado.

print("-"*30)

varios = 1
while varios <= 5:
    while True:
        estado = input(f"Digite el estado del semáforo {varios}: ").lower()
        if estado == "rojo" or estado == "amarillo" or estado == "verde":
            break
        else:
            print("Error: digite un estado válido")

    if estado == "rojo":
        print("*** Pare!!")
    elif estado == "amarillo":
        print("*** Reduzca la velocidad...")
    elif estado == "verde":
        print("*** Continuar...")

    varios += 1

print("Gracias por usar nuestros servicios!!")

print("-"*30)

