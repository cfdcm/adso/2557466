# C - Create, Insert
# Listas en Python

edades = []
edades.append(20)
edades.append("hola")
edades.append(True)

print(edades)
print(len(edades))

edades.insert(0, "ADSO")

print(edades)
print(f"Tamaño de la lista {len(edades)}")


edades = []
for i in range(3):
    edad = int(input(f"Digite la edad {i+1}: "))
    edades.append(edad)

print(edades)


# Update - Modificar valores
edades[0] = 15

print(edades)

tam = len(edades)
for i in range(tam):    # 0,1,2,3,4
    edad = int(input(f"Digite la edad {i+1}: "))
    edades[i] = edad

print(edades)
