# for ... in
# range()     función generadora de números como lista.


#       inicio, fin, paso
# range(start, stop, step)
"""
#    stop
range(20)       # inicia en 0 y termina en 19  -> [0, 1, ..., 18, 19]
range(5)        # inicia en 0 y termina en 4  -> [0, 1, 2, 3, 4]
range(3)        # inicia en 0 y termina en 2  -> [0, 1, 2]
range(1)        # inicia en 0 y termina en 0  -> [0]
range(0)        # inicia en - y termina en -  -> []

"""
print(list(range(20)))
for i in range(20):
    print(i)
