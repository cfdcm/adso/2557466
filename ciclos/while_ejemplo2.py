# While: Modo, pregunta.

# Averiguar el número mágico.

import random

magico = random.randint(1, 10)

num = int(input("Adivina el número (1-10): "))

while num != magico:
    print("Intente de nuevo....")
    num = int(input("Adivina el número (1-10): "))

print("Adivinaste !!!")