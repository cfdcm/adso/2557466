# Programación Orientada a Objetos en Python. POO - OOP
class Dispositivo:
	pass

class Perro:
	pass

# Instanciación de una clase: creación de un objeto
# Instancia == objeto

audifono1 = Dispositivo()
audifono2 = Dispositivo()

toby = Perro()
firulais = Perro()

