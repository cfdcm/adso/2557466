import colorama

class Dispositivo:
	# atributos de clase
	voltaje = 110

	# constructor
	def __init__(self, num, marca, serial):
		# atributos de instancia u objeto específico
		self.num = num
		self.marca = marca
		self.serial = serial
		self.pantalla = False
		self.sonido = "Si"
		# print("Objeto creado correctamente!!")

	def __str__(self):
		return f"Yo soy el audífono # {self.num}"


# crear objetos == instancia
# audif1 = Dispositivo(1, "Sony", "X156465")
# audif2 = Dispositivo(2, "Apple", "A0001")
"""
print(Dispositivo.voltaje)

print(f"{Fore.RED}{audif1}{Style.RESET_ALL}")

print(audif1.marca)
audif1.marca = "Xiaomy"
print(audif1.voltaje)
Dispositivo.voltaje = 120
print(audif1.voltaje)
print(audif1.marca)

print(audif1.serial)

print(f"{Fore.GREEN}{audif1}{Style.RESET_ALL}")
print(Style.RESET_ALL)

print(audif2.marca)
print(audif2.serial)
print("")
print(Dispositivo.voltaje)
print(audif2.voltaje)
"""