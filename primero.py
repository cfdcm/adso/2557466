# Primer ejercicio de Python
# Autor: Jorge García C.


"""
Fecha: 30 de enero 2023
ADSO
Ficha: 22222
Jorge García C.
"""

'''
Hola mundo!!
SENA
'''


print("Hola")
print("ADSO")


# Variables



botella = "agua"	# str	string    cadena    char   varchar   texto
print(botella)

botella = 500		# int	integer   entero
botella = True		# bool  boleano   True|False	verdadero|falso
botella = 0.5		# float decimal   fraccionarios quebrados
botella = None		# None  null


# Indentación		Identación		Formateo	Sangría

var1 = 5
var2 = 10






