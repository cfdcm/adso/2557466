"""
Construya una función que averigüe si una persona puede votar o no. A partir de su edad. Imprimir en la función. No use retorno.

En el programa principal, utilice un ciclo for para solicitar la edad a 5 personas, y usando la función indique el resultado (es decir, si puede votar o no).

"""

# zona de definición de funciones


def puede_votar(e):
    if e > 17:
        print("Usted es mayor de edad y si puede votar")
    else:
        print("Usted no es mayor de edad y no puede votar")


if __name__ == "__main__":
    print("*" * 50)

    for i in range(5):
        edad = int(input("Digite la edad: "))
        puede_votar(edad)

    print("*" * 50)
