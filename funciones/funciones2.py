# 3 forma de uso de funciones. Uso de retorno (return)

def pesos_dolares(pesos, trm):
    result = pesos / trm
    return result


def dolares_pesos(dolares, trm):
    return dolares * trm


if __name__ == "__main__":
    while True:
        opc = int(input("""
        Que desea hacer?
        1. Convertir pesos a dólares
        2. Convertir dólares a pesos
        3. Salir
        """))
        if opc == 1:
            pesos = int(input("Cantidad de pesos a convertir: "))
            trm = int(input("A cómo está el dólar hoy?: "))

            r = pesos_dolares(pesos, trm)
            print(f"Resultado: {r} dólares")
        elif opc == 2:
            dolar = int(input("Cantidad de dólares a convertir: "))
            trm = int(input("A cómo está el dólar hoy?: "))
            print(f"Resultado: {dolares_pesos(dolar, trm)} pesos")
        elif opc == 3:
            break
        else:
            print("Opción inválida... intente de nuevo")

    print("Muchas gracias por usar nuestro software!!")
