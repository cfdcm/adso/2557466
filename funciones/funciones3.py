# Ejemplo 3: Funciones
"""
Simular una tienda. Definir 5 productos. Con su precio.
Ofrecer al usuario lo que desea comprar.
El usuario puede comprar hasta 3 veces.
Preguntar al usuario la cantidad de ese producto que quiere comprar.

Si las unidades a comprar son >= 20, aplicar un 50% descuento. USANDO UNA FUNCIÓN
Si las unidades a comprar son >= 10 y < 20, aplicar un 30% descuento. USANDO UNA FUNCIÓN. ó puede usar la misma función de arriba.
Si las unidades a comprar son >= 1 y < 10, no aplicar descuento.
Calcular subtotales y total final.
"""


def desc_50(valor):
    desc = valor - (valor * 0.5)
    # desc = valor * 0.5
    # desc = valor / 2
    return desc


def desc_30(valor):
    desc = valor - (valor * 0.3)
    # desc = valor * 0.7
    return desc


if __name__ == "__main__":
    total = 0
    for i in range(3):
        op = int(input("""
        1. Pizza                $ 20.000
        2. Pollo asado          $ 40.000
        3. Bandeja Paisa        $ 20.000
        4. Hígado encebollado   $ 15.000
        5. Mondongo             $ 18.000
        """))
        cant = int(input("Digite la cantidad del producto: "))
        # calcular subtotales y total
        subtotal = 0
        if op == 1:
            subtotal = 20000 * cant
        elif op == 2:
            subtotal = 40000 * cant
        elif op == 3:
            subtotal = 20000 * cant
        elif op == 4:
            subtotal = 15000 * cant
        elif op == 5:
            subtotal = 18000 * cant
        else:
            print("Error, producto no existe")

        if cant >= 20:
            print(f"El subtotal ${subtotal} con descuento es: ${desc_50(subtotal)}")
            total += desc_50(subtotal)
        elif cant > 10 and cant < 20:
            print(f"El subtotal ${subtotal} con descuento es: ${desc_30(subtotal)}")
            total += desc_30(subtotal)
        else:
            print(f"El subtotal ${subtotal} no aplica descuento.")
            total += subtotal

    print(f"El total a pagar es: ${total}")
