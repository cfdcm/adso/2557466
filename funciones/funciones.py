# Funciones en Python.
# 1 forma de definición de funciones:
"""
def saludo():
    # Esta función sirve para saludar a los aprendices.
    print("Hola aprendices...!")


saludo()
saludo()
saludo()
"""
# 2 forma, usando argumentos. Sin retorno.


def saludo_especial(nombre):
    """ Esta función sirve para saludar a una persona específica. """
    print(f"Hola {nombre}...!")


"""
saludo_especial("Pedro")
saludo_especial("María")
saludo_especial("a todos, como va el planeta ?")
saludo_especial(50)
saludo_especial(3.5)
saludo_especial(True)
saludo_especial([1, 5, 3])

nombre = input("Digite su nombre: ")
saludo_especial(nombre)

saludo_especial(input("Digite su nombre: "))
"""
for i in range(200):
    saludo_especial("Hola")
