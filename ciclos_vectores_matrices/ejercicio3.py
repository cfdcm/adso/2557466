# **************** Crear un software de ventas. ********************************************************
# Preguntar si se quiere registrar un nuevo cliente o usar uno existente. Usar el vector (Clientes)
# Para registro, pedir los siguientes datos:
#           Cédula, nombre y cantidad de compras (inicialmente en cero).

# Averiguar si el cliente existe o no a través de su cédula.

# Ofrecer un menú, si quiere comprar o ver el estado del cliente.

# Para compras: ===============
# Cada vez que un cliente realice una comprar se debe actualizar el campo de "cantidad de compras" + 1.

# Ofrecer un menú de 5 productos a comprar. Usted los define estáticos.
# Los clientes pueden realizar sus compras (Usar vector compras) en el almacén, para lo cual se debe llenar con:
#                   Cédula (la misma del cliente seleccionado), producto, cantidad y precio.


# Para ver el estado  de un cliente: ====================
# Solicitar cédula para buscarlos en el vector "Clientes" y averiguar la "cantidad de compras".
# Adicionalmente, se deben mostrar todas las compras realizadas por ese cliente, con su subtotal y total histórico.

# Fin
