# Crear una lista (10 elementos) con valores aleatorios, sin repetir ninguno.
# Los valores aleatorios van entre 1 y 50.
import random
nums = []
while len(nums) < 10:
    n = random.randint(1, 50)
    encontrado = False
    for i in nums:
        # averiguo si existe el num en el vector
        if n == i:
            # si existe salgo de la búsqueda...
            print(f"{n} ya existe")
            encontrado = True
            break
    if not encontrado:
        print(f"Guardo {n} en la lista")
        nums.append(n)
print(nums)
