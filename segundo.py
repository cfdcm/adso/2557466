# Algoritmos muy sencillos

# Calcular el sueldo de un trabajador, si gana 5.000 la hora, y trabaja
# 23 días en la empresa. Nota: Se trabajan sólo 5 horas al día.

print("El trabajador gana $5.000/hora")
print("Trabajó 23 días, cada uno de cinco horas")

valorHora = 5000
diasTrabajados = 23
horasDelDia = 5

sueldo = valorHora * diasTrabajados * horasDelDia

# Concatenación			join
#PEP-8 buenas prácticas de escritura de código Python

print("El sueldo del trabajador es: ")
print("$", sueldo, sep="")

print("Hola", end=" ")
print("mundo")




print("El sueldo del trabajador es: $", sueldo, sep="")

print(f"El sueldo del trabajador es: ${sueldo} ")



