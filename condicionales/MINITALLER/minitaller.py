#Mini-taller
"""
1. Construya un programa que dados dos valores X y Y, verifique si las siguientes ecuaciones arrojan el mismo resultado:
a) X + 3Y        
b) 2X - 5Y
Ejemplo: 
x=16, y=2
x=8, y=1 
x=0, y=0
x=-8, y = -1
"""
# x = int(input("Digite el valor de X: "))
# y = int(input("Digite el valor de Y: "))

# ecu1 = x+3*y
# ecu2 = 2*x - 5*y

# if ecu1 == ecu2:
#     print("Arrojan el mismo resultado")
# else:
#     print("Resultados distintos")

"""
2. Averiguar entre dos horas dadas en formato AM/PM, cual es mayor. Pedir las horas así: Horas, Minutos, Meridiano.
Controlar:
las Horas que no superen las 12
los Minutos que no superen los 59
Meridiano, sólo sean: AM|PM.
"""
# horas1 = int(input("Digite la hora (1-12): "))
# min1 = int(input("Digite los minutos (0-59): "))
# mer1 = input("Digite AM|PM: ").lower()

# horas2 = int(input("Digite la hora (1-12): "))
# min2 = int(input("Digite los minutos (0-59): "))
# mer2 = input("Digite AM|PM: ").lower()

# if (horas1 >=1 and horas1 <=12) and (min1 >=0 and min1 <=59) and (mer1=="am" or mer1=="pm") and (horas2 >=1 and horas2 <=12) and (min2 >=0 and min2 <=59) and (mer2=="am" or mer2=="pm"):
#     print(f"Hora1: {horas1}:{min1} {mer1}")
#     print(f"Hora2: {horas2}:{min2} {mer2}")
    
#     if mer1 == "pm" and mer2 != "pm":
#         print(f"La hora Hora1: {horas1}:{min1} {mer1} es la Mayor")
#     elif mer2 == "pm" and mer1 != "pm":
#         print(f"La hora Hora2: {horas2}:{min2} {mer2} es la Mayor")
#     else:
#         if horas1 > horas2:
#             print(f"La hora Hora1: {horas1}:{min1} {mer1} es la Mayor")
#         elif horas2 > horas1:
#             print(f"La hora Hora2: {horas2}:{min2} {mer2} es la Mayor")
#         else:            
#             if min1 > min2:
#                 print(f"La hora Hora1: {horas1}:{min1} {mer1} es la Mayor")
#             elif min2 > min1:
#                 print(f"La hora Hora2: {horas2}:{min2} {mer2} es la Mayor")
#             else:
#                 print("Las horas son iguales.")
# else:
#     print("Error en alguna de las horas")

"""
3. Averiguar entre dos horas dadas en formato militar, cual es menor. Pedir las horas así: Horas, Minutos
Controlar:
las Horas que no superen las 23
los Minutos que no superen los 59
"""
# horas1 = int(input("Digite la hora (0-23): "))
# min1 = int(input("Digite los minutos (0-59): "))

# horas2 = int(input("Digite la hora (0-23): "))
# min2 = int(input("Digite los minutos (0-59): "))

# if (horas1 >=0 and horas1 <=23) and (min1 >=0 and min1 <=59) and (horas2 >=0 and horas2 <=23) and (min2 >=0 and min2 <=59):
#     print(f"Hora1: {horas1}:{min1}")
#     print(f"Hora2: {horas2}:{min2}")
#     if horas1 > horas2:
#         print(f"Mayor Hora1: {horas1}:{min1}")
#     elif horas2 > horas1:
#         print(f"Mayor Hora2: {horas2}:{min2}")
#     else:
#         if min1 > min2:
#             print(f"Mayor Hora1: {horas1}:{min1}")
#         elif min2 > min1:
#             print(f"Mayor Hora2: {horas2}:{min2}")
#         else:
#             print("Las horas son iguales.")
            
# else:
#     print("Error en alguna de las horas")

"""
4. Construir un programa que redondee un número decimal entre 1 y 10, a su equivalente en entero. 
# 4.6   -> 5
# 4.4   -> 4
Nota: Si el decimal es .5 o menor, redondear hacia abajo
Nota: Si el decimal es mayor a .5, redondear hacia arriba
"""

# num = float(input("Digite la nota del taller de algoritmos: "))

# entera = int(num)   # <---- trampa -------, sólo en casos de emergencia.
# print(f"Parte entera {entera}")

# decimal = num - entera
# print(f"Parte decimal {decimal}")

# if decimal > 0.5:
#     entera = entera + 1
# else:
#     entera = entera

# print(f"El número {num} redondeado es: {entera}")

# # version con MOD

# num = float(input("Digite la nota del taller de algoritmos: "))

# decimal = num % 1
# print(f"Parte decimal {decimal}")

# entera = num - decimal
# print(f"Parte entera {entera}")

# if decimal > 0.5:
#     entera = entera + 1
# else:
#     entera = entera

# print(f"El número {num} redondeado es: {entera}")



"""
5. Preguntar al usuario si quiere dibujar un: Cuadrado, Rectángulo, Triángulo o una línea.
Nota usar asteriscos (*)
Ejemplo: Cuadrado.
* * *
*   *
* * *

"""

print("* * *")
print("*   *")
print("* * *")


print("""
* * *
*   *
* * *
""")