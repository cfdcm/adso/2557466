"""Pedir al usuario las 3 notas de algoritmos. Calcular su promedio.

1. Si el promedio es mayor o igual que 3.5, mostrar un mensaje de "Felicidades, ganó!!".

Además calcular un 10% de aumento al promedio en la nota. Sin importar que su promedio podría ser 5.0

2. Si el promedio es menor, entonces indicar que perdió.
"""

nota1 = float(input("Digite su nota1: "))
nota2 = float(input("Digite su nota2: "))
nota3 = float(input("Digite su nota3: "))
prom = (nota1+nota2+nota3)/3

if prom >= 3.5:
    print("Felicidades!!")
    print(f"Te acabas de ganar un aumento: {prom*1.1:0.2f}")
else:
    print(f"Sigue estudiando.. tu promedio: {prom:0.2f}")




