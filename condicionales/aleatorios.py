# Juego. Construya un algoritmo que lanze dos dados, 4 veces. Contar las veces en que la suma de ellos da: 5 y 10. Y además si hubo un pinini o no. 1  y  1

import random
cincos = 0
dieces = 0
pinini = 0

# primera lanzada de dados
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)
suma = dado1 + dado2
print(f"La suma de los dados es: {suma}")

if suma == 5:
    cincos = cincos + 1
elif suma == 10:
    dieces = dieces + 1
elif suma == 2:
    print("Ocurrió un pinini...")
    pinini = pinini + 1
else:
    print("No ocurrió ninguno de los casos en la lanzada 1...")

# segunda lanzada de dados
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"La suma de los dados es: {suma}")

if suma == 5:
    cincos = cincos + 1
elif suma == 10:
    dieces = dieces + 1
elif suma == 2:
    print("Ocurrió un pinini...")
    pinini = pinini + 1
else:
    print("No ocurrió ninguno de los casos en la lanzada 2...")

# tercera lanzada de dados
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"La suma de los dados es: {suma}")

if suma == 5:
    cincos = cincos + 1
elif suma == 10:
    dieces = dieces + 1
elif suma == 2:
    print("Ocurrió un pinini...")
    pinini = pinini + 1
else:
    print("No ocurrió ninguno de los casos en la lanzada 3...")

# cuarta lanzada de dados
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"La suma de los dados es: {suma}")

if suma == 5:
    cincos = cincos + 1
elif suma == 10:
    dieces = dieces + 1
elif suma == 2:
    print("Ocurrió un pinini...")
    pinini = pinini + 1
else:
    print("No ocurrió ninguno de los casos en la lanzada 4...")
    
    
# Resumen final del algoritmo
print("="*50)
print(f"Las veces que salieron 5, son: {cincos}")
print(f"Las veces que salieron 10, son: {dieces}")

if pinini > 0:
    print("Por lo menos ocurrió un pinini")
    print(f"Las veces totales fueron: {pinini}")
else:
    print("No hubo pininis... en las cuatro lanzadas.")