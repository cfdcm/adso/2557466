# Condicional anidado.
#                           2n     2n+1
# Averiguar si un número es par o impar y además averiguar si es positivo o negativo. Dentro de cada caso.

# Residuos, Factores.

num = int(input("Digite un número por favor: "))

if num % 2 == 0:
    print(f"EL num {num} es PAR")
else:
    print(f"EL num {num} es IMPAR")
    if num < 0:
        print("y es negativo")
    else:
        print("y es positivo")
        
    
