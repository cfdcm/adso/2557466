""" operadores lógicos
<       menor que
>       mayor que
<=      menor o igual que
>=      mayor o igual que
==      igual, no confundir con asignación =
!=      diferente
and     y
or      o
not     negación
"""

# Estructura de Control: CONDICIONALES   if

#    True|False
#    condición  entonces
if True :
    # bloque de instrucciones para el caso TRUE
    pass


num1 = int( input("Digite un número: ") )
num2 = int( input("Digite otro número: ") )


if num1 > num2:
    print("num1 es mayor que num2")
print("Gracias por usar nuestro software V1.0")

