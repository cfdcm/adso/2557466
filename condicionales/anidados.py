# Condicionales anidados. 
# Un condicional dentro de otro (en casos)
# Son infinitos

tengo_dinero = "si"
tengo_hambre = "si"
plato_correcto = "si"

if tengo_dinero == "si":
    if tengo_hambre == "si":
        print("Comer lo que sea...")
    else:
        print("Espero a tener hambre....")
else:
    # no tengo dinero
    if tengo_hambre == "si":
        print("Fiar.....")
    else:
        if plato_correcto == "si":
            print("Fio y como por antojo...")
        else:
            print("Me voy para la casa.")