# Construya un algorimo que simule un semáforo.

# Preguntar al usuario en que estádo (color) está el semáforo y mostrar el mensaje correspondiente para cada caso.

estado = input("""Digite el estado del semáforo: 
- Rojo
- Amarillo
- Verde
""").upper()

if estado == "ROJO":
    print("Pare!")
elif estado == "AMARILLO":
    print("Precaución")
elif estado == "VERDE":
    print("Siga...")
else:
    print("Ese estado no existe")
