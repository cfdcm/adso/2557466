# Condicional completo, multicaso.

if True :
    pass
elif True:
    pass
else:
    pass


# Convertir un número entre 1 y 5 a palabras.
# 3   tres
# 5   cinco

num = int( input("Digite un valor entre 1-5: ") )

if num == 1:
    print("Uno")
elif num == 2:
    print("Dos")
elif num == 3:
    print("Tres")
elif num == 4:
    print("Cuatro")
elif num == 5:
    print("Cinco")
else:
    print("Número erróneo.")

    
    