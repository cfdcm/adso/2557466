# Utilizar and , or, not    si es necesario.
# Averiguar si un número es par o impar y además averiguar si es positivo o negativo. Dentro de cada caso.

# Gestión de errores (Excepciones) en python
# try... except

try:
    num = int(input("Digite un número por favor: "))
    if num % 2 == 0 and num >= 0:
        print(f"EL num {num} es PAR y es positivo")
    elif num % 2 == 0 and num < 0:
        print(f"EL num {num} es PAR y es negativo")
    elif num % 2 != 0 and num >= 0:
        print(f"EL num {num} es IMPAR y es positivo")
    elif num % 2 != 0 and num < 0:
        print(f"EL num {num} es IMPAR y es negativo")
    else:
        print("Error...")
except ValueError as e:
    print("Error: Usted no digitó un número.")
except BaseException as e:
    print("Ocurrió un error")


    
