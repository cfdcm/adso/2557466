"""
Construya un software que lea un número por pantalla, si el número es cero.Se debe pedir el nombre.
De lo contrario, pedir el apellido.

Mostrar el número digitado original.
"""

num = int(input("Digite un num: "))

print(f"El número ingresado es: {num}")

if num == 0:
    nom = input("Digite su nombre: ")
    print(f"Gracias {nom}")
else:
    ape = input("Digite su apellido: ")
    print(f"Gracias {ape}")

